//add button listener
document.querySelector('#add-btn').addEventListener('click', calcFunction);
document.querySelector('#substract-btn').addEventListener('click', calcFunction);
document.querySelector('#multiply-btn').addEventListener('click', calcFunction);
document.querySelector('#division-btn').addEventListener('click', calcFunction);
document.querySelector('#square-btn').addEventListener('click', calcFunction);
document.querySelector('#cube-btn').addEventListener('click', calcFunction);
document.querySelector('#remainder-btn').addEventListener('click', calcFunction);


//Calculate function
function calcFunction(e) {

    //get input box values
    let val1 = document.getElementById('value-first').value;
    let val2 = document.getElementById('value-second').value;

    //get button id
    const idName = this.id;

    switch (idName) {
        case 'add-btn':
            const addTotal = +val1 + +val2;
            document.getElementById('result').value = addTotal;
            break;

        case 'substract-btn':
            const subTotal = val1 - val2;
            document.getElementById('result').value = subTotal;
            break;

        case 'multiply-btn':
            const multipleTotal = val1 * val2;
            document.getElementById('result').value = multipleTotal;
            break;

        case 'division-btn':
            const divisionTotal = val1 / val2;
            document.getElementById('result').value = divisionTotal;
            break;

        case 'square-btn':
            const squareTotal = Math.pow(val1, val2);
            document.getElementById('result').value = squareTotal;
            document.getElementById('resultText').innerHTML = `Result ${val1} exponent of ${val2}`;
            break;

        case 'cube-btn':
            const cubeTotal1 = Math.cbrt(val1);
            const cubeTotal2 = Math.cbrt(val2);
            document.getElementById('result').value = `${cubeTotal1}, ${cubeTotal2}`;
            break;

        case 'remainder-btn':
            const remainderTotal = val1 % val2;
            document.getElementById('result').value = remainderTotal;
            break;

        default:
            document.getElementById('result').value = 'No data'
    }

    //Clear fields
    document.getElementById('value-first').value = '';
    document.getElementById('value-second').value = '';

}